import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-personne',
  templateUrl: './personne.component.html',
  styleUrls: ['./personne.component.css']
})
export class PersonneComponent implements OnInit {

  api_url = 'http://localhost:8080/api/personne/1';
  api_url_all = 'http://localhost:8080/api/personne/all';
  api_url_api = 'http://localhost:8080/api/personne/';
  personneAngular: Object;
  listePersonneAngular: JSON;
  listePersonneArray: any;

  constructor(public http: HttpClient) { }

  ngOnInit() {

    this.http.get(this.api_url).subscribe(
      (personneAPI) => this.personneAngular = personneAPI
    )
    
    this.http.get(this.api_url_all).subscribe(
      (listPers) => this.listePersonneArray = listPers
    )


  }




}


